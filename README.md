# frogyproxy

FrogyProxy is a python 3 script that create a squid.conf file and iptables rules to use differents output network devices through the proxy.

* Based on https://github.com/dmegyesi/vpnproxy




```
                |-----------|---out1--- /dev/usb0 Usb lte device
                |           |---out2--- /dev/xxx
localnet---in---| squid     |---out3---
                |           |---out4---
                |-----------|---out5---


```

* Tested and used on a paspberry pi with debian 10



FrogyProxy is a config script to use multiple network output devices. It will create the squid config file and iptables rules to link a network device to a squid port.

You can for example plug some LTE usb devices and use them to scrape sites using different ip address.


# /etc/iproute2/rt_tables
A new table is created for each output device.

It will modify the /etc/iproute2/rt_tables file


# /etc/squid/squid.conf
A new confilg file for squid is created

/etc/squid/squid.conf

# frogyproxy.sh
A new shell script is created. This file contains the ip table rules to link and forward trafic from different proxy ports to the linked network device.

This is started during boot after 60sec. Enough time to let the usb network device to be up and running.

/root/frogyproxy.sh


## Debug / Test

These example are based on:
```
local network    192.168.0.0/24 
    def. gateway 192.168.0.1
    proxy eth0   192.168.0.3

External device
    eth1    lte usb stick   
        net:    192.168.8.0/24
        ip:     192.168.8.8
        gw:     192.168.8.1
    usb0    android usb teethering
    
```


**Testing the devices**
```
ping 8.8.8.8
ping -I eth1 8.8.8.8
ping -I usb0 8.8.8.8

```

**Testing the proxy**

```
curl ifconfig.me
curl --proxy http://192.168.0.3:3128 ifconfig.me
curl --proxy http://192.168.0.3:3129 ifconfig.me
```






